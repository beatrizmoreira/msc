// Type state of the auction
// @auctionEnded: boolean variable which indicates if the auction has ended or not
// @address: address of the user interacting with the contract
// @bid: amount bid by the user interacting with the contract
// @highestBidder: address of the user currently holding the highest bid
type state is record
    auctionEnded: int; //0 is false, 1 is true
    address: int;
    bid: int;
    highestBidder: int;
    highestBid: int;
end

// Structure map in which pending returns are stored
// @key: address of the client's bid
// @value: amount bid by the client
type store is map(int, int);

// const owner : address = ("tz1TGu6TN5GSez2ndXXeDX6LgUDvLzPLqgYV" : address);
// The address of the clients and owner are integers for sake of simplicity
// Owner's address is 0, and client 1 is 1, and so on
const owner: int = 0;


// Entrypoint function needs two args
// @parameters - invocation of operation
// @storage - storage value

// Function Init can only be called by the owner of the contract
// And only turns variable auctionEnded to false (0)
// (meaning the auction is running) if auctionEnded is true (1)
function init(var state: state; var store: store) : ((list(operation) * store)) is
    begin
        if(state.address =/= owner) then
            failwith("Not the owner of the contract");
        else if (state.auctionEnded = 0) then
            failwith("The auction is already running");
        else 
            state.auctionEnded := 1;
    end with ((nil : list(operation)), store)

// Function Finish can only be called by the owner of the contract
// And only turns variable auctionEnded to true (1)
// If auctionEnded is false (0), means that the auction has ended
function finish(var state: state; var store: store) : ((list(operation) * store)) is
    begin
        if (state.address = owner and state.auctionEnded = 0) then
            state.auctionEnded := 1;
        else
            failwith("Cannot terminate auction");
    end with ((nil : list(operation)), store)

// Function bid can only be executed by clients that arent the highest bidder
// While the contract is still running
// It adds the amount bid to the pending returns, and if it is greater than
// The current highest bid, it will update both the highest bid and bidder
// If the client doesnt exist in the store, it fails with failwith("GET_FORCE")
function bid(var state: state; var store: store) : 
                ((list(operation) * store)) is
    begin
        if (state.address = owner) then
            failwith("Owner cannot bid");
        else if (state.auctionEnded = 1) then
            failwith("Auction is over");
        else if (state.address = state.highestBidder) then
            failwith("Client is already the highest bidder");
        else if (get_force(state.address, store) > 0) then
            failwith("Already bid, need to withdraw first");
        else if (state.bid <= 0) then
            failwith("Value of bid needs to be greater than 0")
        else if (state.bid <= state.highestBid) then {
            store[state.address] := (state.bid);
        }
        else{
            state.highestBid := state.bid;
            state.highestBidder := state.address;
            store[state.address] := (state.bid);
        }
    end with ((nil : list(operation)), store)

// Function withdraw can only be executed by clients that arent the highest bidder
// The client had to have bid beforehand
// Removes the value of the bid from the pending returns
// If the client doesnt exist in the store, it fails with failwith("GET_FORCE")
function withdraw(var state: state; var store: store) : ((list(operation) * store)) is
    begin
        if (state.address = owner) then
            failwith("Owner cannot withdraw");
        else if (state.address = state.highestBidder) then
            failwith("Client is the highest bidder, cannot withdraw");
        else if (get_force(state.address, store) = 0) then
            failwith("Client has no pending returns");
        //else if doesnt have any pending return
        else{
            //remove from pending returns
            store[state.address] := 0;
        }
    end with ((nil : list(operation)), store)

// Function win can only be executed by the winner of the auction
// after the auction has ended.
function win(var state: state; var store: store) : ((list(operation) * store)) is
    begin
        if (state.address = owner) then
            failwith("Owner cannot win");
        else if (state.auctionEnded = 0) then
            failwith("Auction is still running");
        else if (state.address =/= state.highestBidder) then
            failwith("Isn't the winner of the auction");
        else if (get_force(state.address, store) = 0) then
            failwith("Prize was already collected");
        else{
            store[state.address] := 0;
        }
    end with ((nil : list(operation)), store)

//some tests
//ligo dry-run auction.ligo bid 'record auctionEnded = 0; address = 1; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'
//ligo dry-run auction.ligo withdraw 'record auctionEnded = 0; address = 1; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'
//ligo dry-run auction.ligo init 'record auctionEnded = 0; address = 1; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'
//ligo dry-run auction.ligo finish 'record auctionEnded = 0; address = 1; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'
//ligo dry-run auction.ligo bid 'record auctionEnded = 0; address = 3; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'
//ligo dry-run auction.ligo win 'record auctionEnded = 0; address = 3; bid = 5; highestBidder = 3; highestBid = 11; end' 'map 1n -> 0;2n -> 0; end'

// function main(const state: state; var store: store) : ((list(operation) * store)) is
//     block {
//     skip
//   } with case state.entryAction of
//     | Init(param) -> init(state,store)
//     | Finish(param) -> finish(state,store)
//     end;  

// function init(const op: int; const store: store) : (list(operation) * store) is
//     begin
//         if (owner = address and auctionEnded = True) then 
//             auctionEnded := False;
//         else
//             failwith("Not the owner");
//     end with (op, store)

// function endAuction(const op: int; const store: store) : (list(operation) * store) is
//     begin
//         if (owner = address and auctionEnded = False) then 
//             auctionEnded := True;
//         else
//             failwith("Not the owner");
// end with (op, store)

// function bid (const operation : int; const store: store) : (list(operation) * store) is 
//     begin
//         const newBid: bid = record 
//             address = address;
//             value = amount;
//         end;
//         if ( address =/= owner and auctionEnded = False) then { 
//             pendingreturns[address] := (address, newBid);
//             if(value > highestBidder.value) then 
//                 highestBid := newBid;
//             else
//                 failwith("Not the highest Bid");
//         }else
//             failwith("The owner cannot bid");
//     end with (operation, store)

// function withdraw (const operation : int; const store: store) : (list(operation) * store) is 
//     begin
//         if(address =/= owner and address =/= highestBidder.address) then {
//             var bidWithdraw: bid := pendingreturns[address];
//             if(bidWitdraw =/= nil) then 
//                 pendingreturns[address] := nil;
//             else
//                 failwith("Has nothing to withdraw");
//         }else
//             failwith("Cannot withdraw");
//     end with (operation, store)
